@extends('peoples.layout')
@section('content')
<div class="container p-5">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Nueva Persona</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-danger" href="{{ route('peoples.index') }}">Cancel·lar</a>
        </div>
    </div>
</div>
  
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Error!</strong>Hi han problemes al teu input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('peoples.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="name" class="form-control">
                @error('name')
                <p class="form-text text-danger">ERROR EN EL NOMBRE</p>
                @enderror
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Edad:</strong>
                <input type="text" name="age" class="form-control">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Cumpleaños:</strong>
                <input type="text" name="birthday" class="form-control" >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comentario:</strong>
                <input type="text" name="comment" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Profession:</strong>
                <input type="text" name="profession" class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-4">
                <button type="submit" class="btn btn-success">Añadir</button>
        </div>
    </div>
   
</form>
</div>

@endsection