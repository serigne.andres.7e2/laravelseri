@extends('peoples.layout')
  
@section('content')




    <div class="container p-5">
      <div class="row ">

        <div class="col-lg-12 ">
                <h2>Datos de {{ $people->name }}</h2>
    </div>
   
        <div >
            <div>
                <strong>Edat:</strong>
                {{ $people->age }}
            </div>
        </div>
        <div>
            <div>
                <strong>Ocupació:</strong>
                {{ $people->profession }}
            </div>
        </div>
        <div>
            <div>
                <strong>Aniversari:</strong>
                {{ $people->birthday }} 
            </div>
        </div>
        <div>
            <div>
                <strong>Comentari adicional:</strong>
                {{ $people->comment }}
            </div>
        </div>
        <div>
        <a class="btn btn-primary" href="{{ route('peoples.index') }}">Tornar</a>
        </div>

    </div>

@endsection