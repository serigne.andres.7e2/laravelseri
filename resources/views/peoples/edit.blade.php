@extends('peoples.layout')
   
@section('content')
<div class="container p-5">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar persona</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-danger" href="{{ route('peoples.index') }}">Cancel·lar</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Error!</strong>Hi han problemes al teu input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('peoples.update',$people->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="name" value="{{ $people->name }}" class="form-control" placeholder="Nom">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Edat:</strong>
                <input type="text" name="age" value="{{ $people->age }}" class="form-control" placeholder="Edat">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Aniversari:</strong>
                <input type="text" name="birthday" value="{{ $people->birthday }}" class="form-control" placeholder="Aniversari">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comentari adicional:</strong>
                <input type="text" name="comment" value="{{ $people->comment }}" class="form-control" placeholder="Comentari">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ocupació:</strong>
                <input type="text" name="profession" value="{{ $people->profession }}" class="form-control" placeholder="Professió">
            </div>
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-4">
              <button type="submit" class="btn btn-success">Guardar Cambios</button>
            </div>
        </div>
   
    </form>
</div>
@endsection