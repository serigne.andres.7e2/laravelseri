@extends('peoples.layout')
@section('content')
    <div class="container p-5">
        <div class="row text-center">
            <h2>Personas</h2>
            <div class="d-flex justify-content-center p-4">
                <a href="{{ route('peoples.create') }}">Nueva Persona</a>
            </div>
        </div>
        <div class="row ">
            <table class="col-12">
                <tr >
                    <th>Nombre</th>          
                    <th>Treball</th>           
                    <th>Interés</th>
                    <th></th>
                </tr>
                @foreach ($peoples as $people)
                <tr>            
                    <td>{{ $people->name }}</td>         
                    <td>{{ $people->profession }}</td>         
                    <td>{{ $people->comment }}</td>
                    <td>
                        <form action="{{ route('peoples.destroy',$people->id) }}" method="POST">
                            <a class="btn btn-secondary" href="{{ route('peoples.show',$people->id) }}">Mostrar</a>
                            <br>
                            <a class="btn btn-secondary" href="{{ route('peoples.edit',$people->id) }}">Editar</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-secondary">Eliminar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div> 
    </div>
   
    

@if ($message = Session::get('success'))
        <div class="alert alert-warning">
            <p>{{ $message }}</p>
        </div>
    @endif
  
    
    {!! $peoples->links() !!}

@endsection